#!/bin/bash

cd ..
docker run --rm -v $(pwd):/app composer/composer update

docker run --rm -it --user=$(id -u):$(id -g) -v `pwd`:/data mcreations/gulp-bower npm update
docker run --rm -it --user=$(id -u):$(id -g) -v `pwd`:/data mcreations/gulp-bower gulp

docker exec -it env_php_1 /code/update/migrate.sh
sleep 5
cd env/
docker-compose restart