<?php
/**
 * Created by PhpStorm.
 * User: vadims
 * Date: 18.6.3
 * Time: 13:07
 */

namespace SRC\User\Services;


use SRC\User\Repository\UsersRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder;
use Symfony\Component\Security\Core\User\User;

class UsersService
{

    private $userRepository;
    private $config;
    private $USER = [
        'username' => "admin",
        'password' => "111"
    ];

    public function __construct(UsersRepository $userRepository, $config)
    {
        $this->userRepository = $userRepository;
        $this->config = $config;
    }

    public function createUser() {
        $user = new User($this->USER['username'], $this->USER['password']);
        $encoderFactory = new EncoderFactory([
            'Symfony\Component\Security\Core\User\UserInterface' => new BCryptPasswordEncoder(13),
        ]);
        $encoder = $encoderFactory->getEncoder($user);
        $username = $this->USER['username'];
        $password = $encoder->encodePassword($user->getPassword(), $user->getSalt());
        $saved = $this->userRepository->createUser($username, $password, 'ROLE_ADMIN');
        if($saved) {
            file_put_contents('php://stdout', print_r('[' . date('Y-m-d H:i:s') . '] ' . 'User created: ' . $username . ' ' . $password));
        }
    }
}