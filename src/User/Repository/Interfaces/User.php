<?php
/**
 * Created by PhpStorm.
 * User: vadims
 * Date: 17.10.5
 * Time: 14:49
 */


namespace SRC\User\Repository\Interfaces;

interface User
{
    const TABLE_USER = "users";

    const ROLE_SEPARATOR = ', ';

    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_USER = 'ROLE_USER';

    const ROLES = [
        User::ROLE_ADMIN => 'Admin',
        User::ROLE_USER => 'User',
    ];


}