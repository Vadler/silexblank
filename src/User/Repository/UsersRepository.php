<?php
/**
 * Created by PhpStorm.
 * User: vadims
 * Date: 18.6.3
 * Time: 13:23
 */

namespace SRC\User\Repository;

use Doctrine\DBAL\Connection;
use PDO;
use SRC\Application\Repository\AppRepository;

class UsersRepository extends AppRepository
{
    const TABLE = 'users';

    public function __construct(Connection $connection)
    {
        parent::__construct($connection, self::TABLE);
    }

    public function getByUsername($username)
    {
        $sql = "
            SELECT id, username, roles, created
            FROM $this->TABLE
            WHERE username = :username;
        ";
        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue("username", $username, PDO::PARAM_STR);
        $stmt->execute();
        $results = $stmt->fetch();

        return $results;
    }

    public function createUser($username, $password, $roles)
    {
        $queryBuilder = $this->connection->createQueryBuilder();
        $queryBuilder
            ->insert($this->TABLE, 'tbl')
            ->values([
                'username' => ':username',
                'password' => ':password',
                'roles' => ':roles'
            ])
            ->setParameter(':username', $username)
            ->setParameter(':password', $password)
            ->setParameter(':roles', $roles);
        return $queryBuilder->execute();
    }


}
