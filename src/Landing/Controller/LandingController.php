<?php
/**
 * Created by PhpStorm.
 * User: vadims
 * Date: 16.18.10
 * Time: 10:55
 */

namespace SRC\Landing\Controller;

use Silex\Application;
use SRC\Application\Services\AppService;

class LandingController
{
    private $app;
    private $appService;

    public function __construct(Application $app, AppService $appService)
    {
        $this->app = $app;
        $this->appService = $appService;
    }

    public function indexAction()
    {
        $this->appService->addGeneralError("General Error");
        $this->appService->addGeneralSuccess("General Success");
        $this->appService->addGeneralInfo("General Info");


        return $this->app['twig']->render('Landing/view/index.twig', [
        ]);
    }
}