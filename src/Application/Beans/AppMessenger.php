<?php

/**
 * Created by PhpStorm.
 * User: vadims
 * Date: 18.23.2
 * Time: 10:09
 */

namespace SRC\Application\Beans;

class AppMessenger
{
    const ERROR = 'error';
    const SUCCESS = 'success';
    const INFO = 'info';

    private $messages;

    public function __construct()
    {
        $this->clearMessages();
    }

    public function addMessage($type, $message)
    {
        if ($type == self::ERROR) {
            $this->messages[self::ERROR][] = $message;
        } else if ($type == self::SUCCESS) {
            $this->messages[self::SUCCESS][] = $message;
        } else if ($type == self::INFO) {
            $this->messages[self::INFO][] = $message;
        }
    }

    public function getMessages()
    {
        $messages = $this->messages;
        $this->clearMessages();
        return $messages;
    }

    public function clearMessages()
    {
        $this->messages = [
            self::ERROR => [],
            self::SUCCESS => [],
            self::INFO => []
        ];
    }


}