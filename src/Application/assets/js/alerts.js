$(function() {
    $('#admin-alert-close, .admin-alert').click(function() {
        alertPopupView.hideAlert();
    });
});

var alertPopupView = {
    popuppromise: 0,

    showError: function (text) {
        console.log('showError');
        $('.admin-alert')
            .addClass('alert-danger')
            .removeClass('hidden')
            .removeClass('alert-success')
        ;
        $('#general-alert-content').html('<strong>Error! </strong>' + text);
        clearTimeout(this.popuppromise);
        // setTimeout(function() {
        //     $('.admin-alert').addClass('hidden');
    //         $('#general-alert-content').empty();
        // }, 3000);
    },
    showSuccess: function(text) {
        console.log('showSuccess');
        $('.admin-alert')
            .addClass('alert-success')
            .removeClass('hidden')
            .removeClass('alert-danger')
        ;
        $('#general-alert-content').html('<strong>Success! </strong>' + text);
        this.popuppromise = setTimeout(function() {
            $('.admin-alert').addClass('hidden');
            $('#general-alert-content').empty();
        }, 3000);
    },
    hideAlert: function () {
        $('.admin-alert').addClass('hidden');
        $('#general-alert-content').empty();
    },
    checkXhr: function (jqXHR) {
        try {
            var data = jqXHR.responseJSON;
            if(!data) {
                // location.reload();
                return false;
            }
        } catch (e) {
            console.log(e);
        }
    }
};




