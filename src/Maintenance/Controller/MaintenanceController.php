<?php
namespace SRC\Maintenance\Controller;

use Silex\Application;

class MaintenanceController
{
    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function indexAction()
    {
        return $this->app['twig']->render('Maintenance/view/index.twig');
    }
}