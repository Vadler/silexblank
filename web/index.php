<?php
// web/index.php
require_once __DIR__.'/../vendor/autoload.php';

use SRC\Application\CustApplication;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$app = new CustApplication();
define("WEBROOT", __DIR__ .'');
define("RESOURCES", __DIR__ .'/../app/Resources');
define("SUCCESS_CODE", 'success');
define("ERROR_CODE", 'error');

require_once __DIR__ . '/../app/Loaders/ConfigLoader.php';
require_once __DIR__ . '/../app/Loaders/AppComponentLoader.php';
require_once __DIR__ . '/../app/Loaders/ServiceLoader.php';
require_once __DIR__ . '/../app/Config/Router.php';


if($app['debug']) {
    ini_set('display_errors', 1);
    error_reporting(-1);
//    ErrorHandler::register();
//    if ('cli' !== php_sapi_name()) {
//        ExceptionHandler::register();
//    }
}

/* Common error handler */
$app->error(function (\Exception $e, Request $request, $code) use ($app) {
    if($app['debug']) {
        var_dump($code);
        var_dump($request->getRequestUri());
        var_dump($e->getMessage());
        var_dump($e->getCode());
        var_dump($e);
    }
    if(in_array('api', explode('/', $request->getRequestUri()))) {
        return new JsonResponse('We are sorry, but something went wrong.', $code);
    }
    return new Response('We are sorry, but something went wrong.', $code);
});
$app->run();
