# README #

# Config Files

app/Config/settings.yml.dist -> app/Config/settings.yml

env/Config/docker-compose.yml.dist -> env/Config/docker-compose.yml

# Installation:
### Require docker, composer, npm, gulp
    composer install
    npm install
    cd /env
    docker-compose up
    cd /updatedoc
    ./update

# Structure
- app - configurations(Router, settings, migrations, console, translations,
-- services and components loaders)
- env - Docker environment (php, nginx, mysql)
- src - Project sources (Controllers, Services, Repositories, views, assets)
- update - update scripts
- web - Public folder with index.php file js and css