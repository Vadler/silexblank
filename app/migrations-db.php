<?php
require_once __DIR__ . '/../vendor/autoload.php';
use Symfony\Component\Yaml\Yaml;

$env = 'dev';
$config = Yaml::parse(file_get_contents(__DIR__."/Config/settings.yml"));

return array(
    'host'      => $config['database']['host'],
    'port'      => $config['database']['port'],
    'driver'    => $config['database']['driver'],
    'charset'   => $config['database']['charset'],
    'dbname'    => $config['database']['dbname'],
    'user'      => $config['database']['user'],
    'password'  => $config['database']['password'],
    'defaultTableOptions' => [ 'charset'=> 'utf8', 'collate' => 'utf8_unicode_ci'],
);