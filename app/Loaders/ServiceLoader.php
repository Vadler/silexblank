<?php
/********************************************************
 * Services
 ********************************************************/
use SRC\Application\Services\AppService;
use SRC\User\Repository\UsersRepository;
use SRC\User\Services\UsersService;

/**
 * Repositories
 */
$app['repositories.UsersRepository'] = function () use($app) {
    return new UsersRepository($app['db']);
};
/**
 * Services
 */
$app['services.AppService'] = function () use($app) {
    return new AppService($app['session']);
};
$app['services.UserService'] = function () use($app) {
    return new UsersService($app['repositories.UsersRepository'], $app['config']);
};