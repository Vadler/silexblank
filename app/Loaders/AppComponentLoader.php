<?php
/**
 * Created by PhpStorm.
 * User: vadims
 * Date: 16.17.11
 * Time: 14:24
 */
use SRC\User\Repository\Interfaces\User as CUser;
use Silex\Provider\HttpFragmentServiceProvider;
use Silex\Provider\SecurityServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\VarDumperServiceProvider;
use Silex\Provider\WebProfilerServiceProvider;
use SRC\User\Services\UserProvider;
use Symfony\Component\Translation\Loader\YamlFileLoader;

$app['debug'] = $config['system']['debug'];
$app['report_location'] = $config['file_locations']['reports'];
$app['buckaroo_action_type'] = $config['buckaroo']['action_type'];

$app->register(new Silex\Provider\LocaleServiceProvider());
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__ . '/../../src',
));
$cookie_secure = '';
if(!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443)                                                                                          {
    $cookie_secure = true;
}

$app->register(new Silex\Provider\SessionServiceProvider());
$app['session.storage.options'] = [
    'cookie_lifetime' => 1800, //30min
    'cookie_secure' => $cookie_secure,
    'cookie_httponly' => true
];
if(empty($app['session']->get('_locale'))) {
    $app['session']->set('_locale', $config['system']['default_locale']);
}
$app['session']->set('_env', $config['system']['env']);
$app['session']->set('_version', $config['system']['version']);
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'dbs.options' => array (
        'default' => array(
            'driver'    => $config['database']['driver'],
            'host'      => $config['database']['host'],
            'dbname'    => $config['database']['dbname'],
            'user'      => $config['database']['user'],
            'password'  => $config['database']['password'],
            'charset'   => $config['database']['charset'],
        )
    ),
));
$app->register(new Silex\Provider\SwiftmailerServiceProvider());
$app['swiftmailer.options'] = array(
    'host'          => $config['email']['host'],
    'port'          => $config['email']['port'],
    'username'      => $config['email']['username'],
    'password'      => $config['email']['password'],
    'encryption'    => $config['email']['encryption'],
    'auth_mode'     => $config['email']['auth_mode']
);
$app->register(new ServiceControllerServiceProvider());
$app->register(new HttpFragmentServiceProvider());
$app->register(new VarDumperServiceProvider());
$app->register(new SecurityServiceProvider(), [
    'security.firewalls' => [
        'secured' => [
            'pattern' => '^.*$',
            'anonymous' => true,
            'form' => array(
                'login_path' => '/login',
                'check_path' => '/admin/login_check',
            ),
            'logout' => array('logout_path' => '/admin/logout',
                'target_url' => '/admin/dashboard'),
            'users' => function () use ($app) {
                return new UserProvider($app['db']);
            },
        ],
    ],
    'security.access_rules' => array(
        array('^/admin/dashboard', array_keys(CUser::ROLES)),
        array('^.*$', 'IS_AUTHENTICATED_ANONYMOUSLY')
    )
]);

if($app['config']['system']['debug'] && $app['config']['system']['env'] == "test") {
    $app->register(new WebProfilerServiceProvider(), array(
        'profiler.cache_dir' => __DIR__.'/../cache/profiler',
        'profiler.mount_prefix' => '/_profiler', // this is the default
    ));
}

$app->register(new Silex\Provider\TranslationServiceProvider(), [
    'locale_fallback' => $config['system']['default_locale']
]);
$app->extend('translator', function($translator, $app) {
    $translator->addLoader('yaml', new YamlFileLoader());
    if(is_file(RESOURCES.'/translations/generated_en.yml')) {
        $translator->addResource('yaml', __DIR__ . '/../Resources/translations/generated_en.yml', 'en');
    } else {
        $translator->addResource('yaml', __DIR__ . '/../Resources/translations/en.yml', 'en');
    }
    return $translator;
});
