<?php
use Symfony\Component\Yaml\Yaml;

/********************************************************
 * Configuration
 ********************************************************/


$config = Yaml::parse(file_get_contents(__DIR__."/../Config/settings.yml"));
$configCore = Yaml::parse(file_get_contents(__DIR__."/../Config/core.settings.yml"));
$config = array_merge_recursive($config, $configCore);
$app['config'] = $config;
