<?php

use SRC\Application\Controller\AppController;
use SRC\Landing\Controller\LandingController;
use SRC\Maintenance\Controller\MaintenanceController;

/********************************************************
 * Routs/Controller
 ********************************************************/

//A before application middleware allows you to tweak the Request before the controller is executed:
$app['controller.app'] = function () use ($config, $app) {
    return new AppController($app, $config, $app['services.AppService']);
};
    $app->before('controller.app:before');
    //An after application middleware allows you to tweak the Response before it is sent to the client:
    $app->after('controller.app:after');
/**********
 * AppController
 *********/
    $app->get('/language/{locale}', 'controller.app:changeLocale')->bind('changeLocale');

/**********
 * Landing
 *********/
$app['controller.landing'] = function () use ($app) {
    return new LandingController($app, $app['services.AppService']);
};
    $app->get('/', 'controller.landing:indexAction')->bind('landing');

/**********
 * Maintenance
 *********/
    $app['controller.maintenance'] = function () use ($app) {
        return new MaintenanceController($app);
    };
    if($config['system']['maintenance']) {
        $app->get('/maintenance', 'controller.maintenance:indexAction')->bind('maintenance');
    } else {
        $app->get('/maintenance', function() use($app) {
            $uri = $app['url_generator']->generate('landing');
            return $app->redirect($uri);
        });
    }
